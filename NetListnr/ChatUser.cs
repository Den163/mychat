﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetListnr
{
    public delegate TcpClient GetClientObject();

    public abstract class ChatUser
    {
		public IPEndPoint ipEndPoint { get; private set; }

        private MessageSender sender;
        private MessageReceiver receiver;

        protected ChatUser(string ip, int port)
        {
            CreateEndPoint(ip, port);
            CreateUser();

            sender = new MessageSender(GetClient);
            receiver = new MessageReceiver(GetClient);

            InvokeAppCycle();
        }

		private void CreateEndPoint(string ip, int port)
		{
			ipEndPoint = new IPEndPoint(
				IPAddress.Parse(ip),
				port
			);
		}

        private void InvokeAppCycle()
        {
            while(true) {}
        }

        protected abstract void CreateUser();
        protected abstract TcpClient GetClient();
    }
}
