﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetListnr
{
    public class ChatHost: ChatUser
    {
        private TcpListener listener;

        public ChatHost(string ip, int port): base(ip, port) {}

        protected override void CreateUser()
        {
			listener = new TcpListener(ipEndPoint);
			listener.Start();
        }

        protected override TcpClient GetClient()
        {
            return listener.AcceptTcpClient();
        }
    }
}
