﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetListnr
{
    public class MessageSender
    {
        GetClientObject GetClientCallBack;

        public MessageSender(GetClientObject GetClientCallBack)
        {
			this.GetClientCallBack = GetClientCallBack;
            StartThread();
        }

        private void StartThread()
        {
			ThreadPool.QueueUserWorkItem(
				(obj) =>
				{
					while (true)
					{
						Send();
					}
				}
			);  
        }

        private void Send()
        {
			var client = GetClientCallBack();

			using (var netStream = client.GetStream())
			{
                var text = "I: " + Console.ReadLine();
                var sendBytes = Encoding.UTF8.GetBytes(text);

                netStream.Write(sendBytes, 0, sendBytes.Length);

                Console.WriteLine(text);
			}
        }
    }
}
