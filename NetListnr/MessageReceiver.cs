﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetListnr
{
    public class MessageReceiver
    {
        GetClientObject GetClientCallBack;

        public MessageReceiver(GetClientObject GetClientCallback)
        {
            this.GetClientCallBack = GetClientCallback;
            StartThread();
        }

        private void StartThread()
        {
	        ThreadPool.QueueUserWorkItem(
			    (obj) =>
			    {
			        while (true)
			        {
			            Receive();
			        }
			    }
			);
        }

        private void Receive()
        {
            var client = GetClientCallBack();

            using (var netStream = client.GetStream())
            {
                var BYTES_LENGTH = 1028;
                var receivedBytes = new byte[BYTES_LENGTH];
                netStream.Read(receivedBytes, 0, BYTES_LENGTH);

                var text = Encoding.UTF8.GetString(receivedBytes);
                Console.WriteLine("Companion: " + text);
            }
        }
    }
}
