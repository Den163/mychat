﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetListnr
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            try {
                CreateAndStartChatUser();
            }
            catch (Exception e) {
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }

        private static void CreateAndStartChatUser()
        {
			var listener = new ChatHost(
				"192.168.0.108",
				5001
			);
        }
    }
}
