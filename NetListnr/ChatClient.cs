﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetListnr
{
    public class ChatClient: ChatUser
    {
        private TcpClient client;

        public ChatClient(string ip, int port): base(ip, port) { }

		protected override void CreateUser()
		{
			client = new TcpClient();
			client.Connect(ipEndPoint);
		}

		protected override TcpClient GetClient()
		{
            return client;
		}
    }
}
