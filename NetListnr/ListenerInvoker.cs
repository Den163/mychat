﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetListnr
{
    public class ListenerInvoker
    {
        public IPEndPoint EndPoint { get; private set; }

        TcpClient client;
        TcpListener listener;


        public ListenerInvoker(string ip, int port)
        {
            CreateEndPoint(ip, port);
            CreateListener();
            ListenToMessages();
        }

        private void CreateEndPoint(string ip, int port) {
            EndPoint = new IPEndPoint(
                IPAddress.Parse(ip),
                port
            );
        }

        private void CreateListener() 
        {
            listener = new TcpListener(EndPoint);
            listener.Start();
            client = listener.AcceptTcpClient();
        }

        private void ListenToMessages()
        {
            while (true)
            {
                ReadMessageFromClient();
            }
        }

        private void ReadMessageFromClient()
        {
			using (var netStream = client.GetStream())
			{
                var BYTES_LENGTH = 1000;
                var receivedBytes = new byte[BYTES_LENGTH];
                netStream.Read(receivedBytes, 0, BYTES_LENGTH);

                var text = Encoding.UTF8.GetString(receivedBytes);
				Console.WriteLine(text);
			}
        }
    }
}
